#pragma once

#include <gmock/gmock.h>
#include "CommonEnvironment/ITransport.hpp"

namespace common
{

struct ITransportMock : public ITransport
{
    ITransportMock();
    ~ITransportMock() override;

    MOCK_METHOD1(registerMessageCallback, void(MessageCallback));
    MOCK_METHOD1(registerDisconnectedCallback, void(DisconnectedCallback));
    MOCK_METHOD1(sendMessage, bool(BinaryMessage));
    MOCK_CONST_METHOD0(addressToString, std::string());
};

}
