#include <gmock/gmock.h>
#include "Logger/ILogger.hpp"

namespace common
{

struct ILoggerMock : public ILogger
{
    ILoggerMock();
    ~ILoggerMock() override;

    MOCK_METHOD2(log, void(ILogger::Level, const std::string& message));
};

} // namespace common
