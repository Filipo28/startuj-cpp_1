#pragma once

#include <gmock/gmock.h>
#include "UeConnection/IUeConnection.hpp"
#include "Printers/UeSlotPrint.hpp"

namespace bts
{

class IUeConnectionMock : public IUeConnection
{
public:
    IUeConnectionMock();
    ~IUeConnectionMock() override;

    MOCK_METHOD1(start, void(UeSlot ueSlot));
    MOCK_METHOD1(sendMessage, void(BinaryMessage message));
    MOCK_METHOD1(sendSib, void(BtsId btsId));
    MOCK_CONST_METHOD0(getPhoneNumber, PhoneNumber());
    MOCK_CONST_METHOD0(isAttached, bool());
    MOCK_CONST_METHOD1(print, void(std::ostream&));
};


}
